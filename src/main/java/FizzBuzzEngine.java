public class FizzBuzzEngine {
    public static void showFizzBuzz(int number) {
        FizzBuzzListFactory.createList(number).stream().map(FizzBuzzConverter::convert).forEach(System.out::println);
    }
}
