import java.util.ArrayList;
import java.util.List;

public class FizzBuzzListFactory {
    public static List<Integer> createList(int number) {
        if(number < 1)
            throw new IllegalArgumentException("Number should be grater or equal to 1");
        List<Integer> result = new ArrayList<>();
        for(int i=1; i<=number; i++)
            result.add(i);
        return result;
    }
}
