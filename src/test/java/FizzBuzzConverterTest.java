import org.junit.Assert;
import org.junit.Test;

public class FizzBuzzConverterTest {
    @Test
    public void shouldReturn1WhenGiven1(){
        String result = FizzBuzzConverter.convert(1);
        Assert.assertEquals("1", result);
    }

    @Test
    public void shouldReturn2WhenGiven2(){
        String result = FizzBuzzConverter.convert(2);
        Assert.assertEquals("2", result);
    }

    @Test
    public void shouldReturnFizzWhenGiven3(){
        String result = FizzBuzzConverter.convert(3);
        Assert.assertEquals("Fizz", result);
    }

    @Test
    public void shouldReturnFizzWhenGiven6(){
        String result = FizzBuzzConverter.convert(6);
        Assert.assertEquals("Fizz", result);
    }

    @Test
    public void shouldReturnBuzzWhenGiven5(){
        String result = FizzBuzzConverter.convert(5);
        Assert.assertEquals("Buzz", result);
    }

    @Test
    public void shouldReturnBuzzWhenGiven10(){
        String result = FizzBuzzConverter.convert(10);
        Assert.assertEquals("Buzz", result);
    }

    @Test
    public void shouldReturnFizzBuzzWhenGiven15(){
        String result = FizzBuzzConverter.convert(15);
        Assert.assertEquals("FizzBuzz", result);
    }

    @Test
    public void shouldReturnFizzWhenGiven30(){
        String result = FizzBuzzConverter.convert(30);
        Assert.assertEquals("FizzBuzz", result);
    }
}
