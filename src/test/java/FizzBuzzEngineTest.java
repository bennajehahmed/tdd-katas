import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class FizzBuzzEngineTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void shouldPrint1WhenGiven1(){
        FizzBuzzEngine.showFizzBuzz(1);
        Assert.assertEquals( "1", systemOutRule.getLog().trim());
    }

    @Test
    public void shouldPrint12WhenGiven2(){
        FizzBuzzEngine.showFizzBuzz(2);
        Assert.assertEquals( "1\r\n2", systemOutRule.getLog().trim());
    }

    @Test
    public void shouldPrint12FizzWhenGiven3(){
        FizzBuzzEngine.showFizzBuzz(3);
        Assert.assertEquals( "1\r\n2\r\nFizz", systemOutRule.getLog().trim());
    }

}
