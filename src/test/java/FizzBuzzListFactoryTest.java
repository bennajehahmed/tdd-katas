import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class FizzBuzzListFactoryTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenGivenANumberLessOrEqualToZero(){
         FizzBuzzListFactory.createList(0);
    }

    @Test
    public void shouldReturnAListOf1WhenGiven1(){
        List<Integer> result = FizzBuzzListFactory.createList(1);
        Assert.assertEquals(List.of(1), result);
    }

    @Test
    public void shouldReturnAListOf12WhenGiven2(){
        List<Integer> result = FizzBuzzListFactory.createList(2);
        Assert.assertEquals(List.of(1, 2), result);
    }

}
